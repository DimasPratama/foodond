# Installed modules instructions
### Note: Do not do 'react-native link'

## Branch information:
1. master -> branch for base project
2. test_module -> branch for testing modules

## Instructions for Git:
1. Clone the project
2. Delete .git file '**rm -rf .git**'
3. Initialise .git inside '**git init**'
4. Add remote '**git remote add origin <link-to-remote-repo>**'
5. Push upstream master '**git push -u origin master**'
6. Do not forget Gitflow (**NEVER** push directly to master)
7. **頑張って!**

## Instructions for running and building:
### Android (in case debug.keystore is missing)
1. Download template from [this link](https://raw.githubusercontent.com/facebook/react-native/master/template/android/app/debug.keystore)
2. Navigate to android/app directory '**cd android/app**'
3. Drag and drop the downloaded template here
3. Run android '**react-native run-android**'

### iOS
1. Navigate to ios directory '**cd ios/**'
2. Update Cocoapods '**sudo gem install cocoapods**'
3. Install pods and update repo '**pod install --repo-update**'
4. Run iOS '**react-native run-ios**'

## List of modules installed:
1. react-navigation
2. react-native-gesture-handler
3. react-native-firebase
4. react-native-maps
5. @react-native-community/geolocation
